const { executeWithLog } = require('./executeWithLog')

async function updateVersion() {
  try {
    await executeWithLog('npm version minor')
  } catch (error) {
    updateVersion()
  }
}

async function publish() {
  try {
    const commands = ['npm run compile', 'git merge origin/develop', 'git stage --all', 'git commit -m "compilar"']
    await executeWithLog(commands)
    await updateVersion()
    await executeWithLog('git flow publish')
  } catch (error) {
    if (error.stderr) console.error(error.stderr)
    else if (error.stdout) console.error(error.stdout)
    else console.error(error)
  }
}

publish()
