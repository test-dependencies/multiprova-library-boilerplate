const util = require('util')
const execute = util.promisify(require('child_process').exec)

executeSingleCommand = async command => {
  const { stdout, stderr } = await execute(command)
  if (stderr) console.error(stderr)
  if (stdout) console.log(stdout)
}

exports.executeWithLog = async commands => {
  if (Array.isArray(commands)) {
    for (let index in commands) {
      await executeSingleCommand(commands[index])
    }
  } else {
    await executeSingleCommand(commands)
  }
}
